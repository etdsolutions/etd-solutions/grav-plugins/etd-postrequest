#Changelog
Tous les changements notables de ce projet seront référencés dans ce document.

## [0.1.0] - 2018-11-28
### Création
- Création de la connexion à l'API
- Création de champs avec valeurs spécifiques obligatoires dans la liaison API
