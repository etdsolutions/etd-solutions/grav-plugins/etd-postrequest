<?php

namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

/**
 * @package Grav\Plugin
 */
class EtdPostrequestPlugin extends Plugin
{

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onFormProcessed' => ['onFormProcessed', 0]
        ];
    }

    /**
     * Send data to API.
     *
     * @param Event $event
     */
    public function onFormProcessed(Event $event)
    {

        $form = $event['form'];
        $action = $event['action'];
        $params = $event['params'];



        switch ($action) {
            case 'etd-postrequest':

                if ($this->config->get('plugins.etd-postrequest.link')){


                    // On prépare le tableau des fields de la requête
                    $fields = array();

                    // On récupère les champs spécifiques à ajouter dans la requête
                    if($this->config->get('plugins.etd-postrequest.specificfields')){
                        foreach ($this->config->get('plugins.etd-postrequest.specificfields') as $field){
                            $fields[$field['name']] = $field['value'];
                        }
                    }

                    foreach ($form->getData()->toArray() as $index => $item){
                        $fields[$index] = $item;
                    }


                    // On fait une requête vers l'API
                    //The url you wish to send the POST request to
                    $url = $this->config->get('plugins.etd-postrequest.link');

                    //url-ify the data for the POST
                    $fields_string = http_build_query($fields);

                    //open connection
                    $ch = curl_init();

                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, count($fields));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                    //So that curl_exec returns the contents of the cURL; rather than echoing it
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

                    //execute post
                    $result = curl_exec($ch);

                    var_dump($result);

                    break;
                } else {
                    var_dump('Impossible de contacter le service');
                }


        }
    }


}
