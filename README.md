# Grav Request Post Plugin

## About

The **Request Post** plugin send fields to an API.

## Installation

Typically a plugin should be installed via [GPM](http://learn.getgrav.org/advanced/grav-gpm) (Grav Package Manager):

```
$ bin/gpm install etd-postrequest
```

Alternatively it can be installed via the [Admin Plugin](http://learn.getgrav.org/admin-panel/plugins)

## Configuration

```
enabled: true
```

## Quick Example

```

```

## Available Parameters